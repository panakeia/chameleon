{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances  #-}

import           Codec.Picture
import           Codec.Picture.Extra
import           Codec.Picture.Types
import qualified Data.ByteString.Char8        as B
import           Data.Default
import qualified Data.List                    as L
import qualified Data.Maybe                   as M
import qualified Data.Vector.Storable         as VS
import           Network.HTTP.Req
import           Options.Applicative
import qualified System.Console.Terminal.Size as S
import qualified System.Drawille              as D
import           Text.Printf                  (printf)

-- This program is based on:
-- https://github.com/Drezil/img2ascii
-- https://github.com/yamadapc/haskell-drawille/blob/master/examples/Image2Term.hs

-- Implement conversion from RGB pixels to greyscale pixels.
instance ColorConvertible PixelRGB8 Pixel8 where
  promotePixel = computeLuma

data Charset = Braille | Whitespace deriving Eq

data Color   = Full | Greyscale | Silhouette

data Options = Options
  { src       :: String
  , threshold :: Int
  , charset   :: Charset
  , color     :: Color
  }

charsetOpt :: Parser Charset
charsetOpt =
      flag Braille Braille
        (  long "braille"
        <> short 'b'
        <> help "Display image using braille characters (default, requires UTF-8 support)"
        )
  <|> flag' Whitespace
        (  long "no-text"
        <> short 'n'
        <> help "Don't display any text, just colors"
        )

colorOpt :: Parser Color
colorOpt =
      flag Full Full
        (  long "full-color"
        <> short 'f'
        <> help "Display image in full color (default, requires TrueColor support)"
        )
  <|> flag' Greyscale
        (  long "greyscale"
        <> short 'g'
        <> help "Display image in greyscale"
        )
  <|> flag' Silhouette
        (  long "silhouette"
        <> short 's'
        <> help "Display image in silhouette"
        )

options :: Parser Options
options = Options
  <$> argument str
        (  metavar "src"
        <> help "Source file or image URL"
        )
  <*> argument auto
        (  metavar "threshold"
        <> value 196
        <> help "Threshold luminance for displaying braille 'pixels'. Only affects output when -b is set."
        )
  <*> charsetOpt
  <*> colorOpt

opthelp :: ParserInfo Options
opthelp =
  info (helper <*> options)
       (failureCode 1)

main :: IO ()
main = execParser opthelp >>= run

run :: Options -> IO ()
run (Options src thresh charset color) = do
  imageOrErr <- readImageFrom src

  case imageOrErr of
    Left  err -> putStrLn err
    Right img -> do
      case extractDynImage img of
        Just image -> do
          (width, height) <- getDimensions image

          let pixelizeFn = if charset == Whitespace
                               then pixelizeFlat
                               else pixelize

          -- Resize and convert the image to greyscale to create the
          -- drawille canvas, then print the result.
          case pixelizeFn width height image of
            Just (fg, bg) -> do
              let canvas =
                    fromImage
                      (promoteImage $ scaleBilinear (width * 2) (width * 2) image)
                      thresh

                  result =
                    case charset of
                      Braille    -> D.frame canvas
                      Whitespace -> unlines $ take height $ repeat $ replicate width ' '

              putStr $
                case color of
                  Full       -> colorize result (fg, bg)
                  Greyscale  -> colorize result (toGreyscale fg, toGreyscale bg)
                  Silhouette -> result
            Nothing    -> fail "Couldn't downsample image."
        Nothing    -> fail "Couldn't parse image."

-- Make an HTTP request for the image if a URL is provided
-- as the src argument, otherwise try to read from file
readImageFrom :: String -> IO (Either String DynamicImage)
readImageFrom src =
  case parseUrl $ B.pack src of
    Just e  ->
      either
        (\(url, _) -> requestImage url)
        (\(url, _) -> requestImage url)
        e
    Nothing -> readImage src

-- Make an HTTP request and attempt to decode it as an Image
requestImage :: Url scheme -> IO (Either String DynamicImage)
requestImage url = do
  bs <- runReq def $ req GET url NoReqBody bsResponse mempty
  pure . decodeImage $ responseBody bs

-- Given an Image, return an IO tuple of ints representing
-- dimensions to render the image at.
getDimensions :: Pixel a => Image a -> IO (Int, Int)
getDimensions (Image width height _) = do
  win <- S.size

  let aspectRatio     = (fromIntegral width / fromIntegral height)::Double
      heightToWidth h = floor $ (fromIntegral h) * aspectRatio * 2
      widthToHeight w = floor $ (fromIntegral w) / aspectRatio / 2

      fitTo window
        | heightToWidth (S.height window) > (S.width window) =
            (S.width window, widthToHeight (S.width window))
        | heightToWidth (S.height window) < (S.width window) =
            (heightToWidth (S.height window), S.height window)
        | otherwise =
            (S.width window, S.height window)

  pure $ case win of
    Just window -> fitTo window
    Nothing     -> (heightToWidth 24, 24)

-- Given a string and a pair of images, wrap every character in the string
-- in an pair of ANSI color codes - the foreground is based on the first image,
-- the background on the second.
--
-- For best results, ensure the input string is rectangular - i.e., that each
-- line of the string has the same length. Further, both images should have the
-- same dimensions - which must be greater than those of the string.
colorize :: String -> (Image PixelRGB8, Image PixelRGB8) -> String
colorize str (fg@(Image width height _), bg) =
  let strs = lines str

      -- Wrap a character in ANSI color codes (foreground, background)
      colorizeChar c (PixelRGB8 fr fg fb, PixelRGB8 br bg bb) =
        printf "\x1b[48;2;%d;%d;%dm\x1b[38;2;%d;%d;%dm%c" br bg bb fr fg fb c
   in
      (++)
        (unlines
          [ concat
            [ colorizeChar (strs !! y !! x) (pixelAt fg x y, pixelAt bg x y)
            | x <- [ 0..width  - 1 ]]
            | y <- [ 0..height - 1 ]])
        "\x1b[0m" -- ANSI color terminator

-- Folds an image to a pair of images, corresponding to the
-- lightest and darkest pixels in each window.
pixelize tw th img = do
  lightImg <- pixelizeH tw th img (foldPixels lighter)
  darkImg  <- pixelizeH tw th img (foldPixels darker)
  pure (lightImg, darkImg)

-- Folds an image to a pair of identical images, corresponding to
-- the central pixel of each window.
pixelizeFlat tw th img = do
  img <- pixelizeH tw th img (\img cx cy _ _ -> pixelAt img cx cy)
  pure (img, img)

-- Provided an image, output dimensions, and a function which chooses
-- a pixel inside a window of pixels, return a Just image if the window
-- is smaller than the image and Nothing otherwise.
pixelizeH
  :: Pixel a
  => Int
  -> Int
  -> Image a
  -> (Image a -> Int -> Int -> Double -> Double -> a)
  -> Maybe (Image a)
pixelizeH toWidth toHeight image@(Image fromWidth fromHeight _) processWindow
  | fromWidth  < toWidth  = Nothing
  | fromHeight < toHeight = Nothing
  | otherwise =
      let windowWidth  = fromIntegral fromWidth  / fromIntegral toWidth
          windowHeight = fromIntegral fromHeight / fromIntegral toHeight

          -- Fold pixel values in the window surrounding (x, y) to a single pixel
          foldFn acc x y =
            let cx = floor $ fromIntegral x * windowWidth  -- initial window center
                cy = floor $ fromIntegral y * windowHeight
            in
                (,)
                  acc
                  (processWindow image cx cy windowWidth windowHeight)
       in
          Just . snd $ generateFoldImage foldFn image toWidth toHeight

-- Given a function to combine two pixels, fold all of the pixels
-- in the window into a single pixel.
foldPixels
  :: Pixel a
  => (a -> a -> a)
  -> Image a
  -> Int
  -> Int
  -> Double
  -> Double
  -> a
foldPixels foldFn img cx cy windowWidth windowHeight =
  L.foldl' foldFn
    (pixelAt img cx cy)
    (pixelsIn img cx cy windowWidth windowHeight)

-- Return the set of pixels in a window around the given coordinates.
pixelsIn :: Pixel a => Image a -> Int -> Int -> Double -> Double -> [a]
pixelsIn image x y width height =
  let dw = floor width                             -- window dimensions
      dh = floor height
   in
      [ pixelAt image (x + dx) (y + dy)
      | dx <- [-dw..dw]                            -- x and y coordinate ranges
      , dy <- [-dw..dw]
      , x + dx > 0 && x + dx < (imageWidth  image)
      , y + dy > 0 && y + dy < (imageHeight image)
      ]

-- Convert an RGB image to grayscale in the RGB color space
toGreyscale :: Image PixelRGB8 -> Image PixelRGB8
toGreyscale img =
  promoteImage (promoteImage img :: Image Pixel8) :: Image PixelRGB8

-- Return the brighter of two Pixels.
lighter :: PixelRGB8 -> PixelRGB8 -> PixelRGB8
lighter x y =
  if computeLuma x > computeLuma y
     then x
     else y

-- Return the darker of two Pixels.
darker :: PixelRGB8 -> PixelRGB8 -> PixelRGB8
darker x y =
  if computeLuma x < computeLuma y
     then x
     else y

-- Extract the Image out of a DynamicImage.
extractDynImage :: DynamicImage -> Maybe (Image PixelRGB8)
extractDynImage image =
    case image of
        ImageY8 img     -> Just $ promoteImage img
        ImageYA8 img    -> Just $ promoteImage img
        ImageRGB8 img   -> Just img
        ImageRGBA8 img  -> Just $ pixelMap dropTransparency img
        ImageYCbCr8 img -> Just $ convertImage img
        ImageCMYK8 img  -> Just $ convertImage img
        _               -> Nothing

-- Converts a greyscale image to a Canvas. The threshold parameter is the
-- luminance value above which a Canvas pixel will be hidden.
fromImage :: Image Pixel8 -> Int -> D.Canvas
fromImage (Image width height imageData) threshold =
  let -- set the corner coordinates so the canvas has the correct dimensions
      canvas  = D.set   (D.set   D.empty (0, 0)) (width, height)

      -- unset the corner coordinates
      canvas' = D.unset (D.unset canvas  (0, 0)) (width, height)
   in
      VS.ifoldr (accCanvas threshold width) canvas' imageData

-- Accumulate pixels into a canvas.
accCanvas :: Int -> Int -> Int -> Pixel8 -> D.Canvas -> D.Canvas
accCanvas threshold width idx pixel canvas =
  if pixel < fromIntegral threshold
     then D.set canvas (coord width idx)
     else canvas

-- Gets a canvas coordinate given a canvas' width and a index to the flat
-- vector representation of an image's pixel matrix.
coord :: Int -> Int -> (Int, Int)
coord width idx = (idx `rem` width, idx `div` width)
