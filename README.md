# chameleon

Display images in the terminal in various ways. Requires a terminal emulator which supports utf-8 and true color.

Based on [Drezil/img2ascii](https://github.com/Drezil/img2ascii) and [yamadapdc/Image2Term](https://github.com/yamadapc/haskell-drawille/blob/master/examples/Image2Term.hs).

## Example

![](examples/example.png)

## Usage
`stack build`

`stack exec -- chameleon-exe src [threshold] ([-b|--braille] | [-n|--no-text])
([-f|--full-color] | [-g|--greyscale] | [-s|--silhouette])`

## To do
 - add option for manually specifying output dimensions
 - allow text output in ASCII
 - add 256-color and 16-color modes
 - think of other display modes
